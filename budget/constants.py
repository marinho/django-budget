from django.utils.dates import MONTHS
from django.utils.dates import WEEKDAYS


MONTH_CHOICES = [(None, 'Any month')] + MONTHS.items()
DAY_CHOICES = [(d, unicode(d)) for d in range(1,32)]
WEEKDAY_CHOICES = WEEKDAYS.items()


OPERATION_INCOME = 1
OPERATION_OUTCOME = -1
OPERATION_CHOICES = (
    (OPERATION_INCOME, 'Income'),
    (OPERATION_OUTCOME, 'Outcome'),
)


REGULARITY_ONCE = 'once'
REGULARITY_MONTHLY = 'monthly'
REGULARITY_YEARLY = 'yearly'
REGULARITY_CHOICES = (
    (REGULARITY_ONCE, 'Once'),
    (REGULARITY_MONTHLY, 'Monthly'),
    (REGULARITY_YEARLY, 'Yearly')
)
