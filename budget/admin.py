from datetime import datetime, timedelta
from django.contrib import admin
from django.template.response import SimpleTemplateResponse
from django.contrib.admin.options import BaseModelAdmin
from django.contrib.admin.options import csrf_protect_m
from django.contrib.admin.options import IncorrectLookupParameters
from django.http import HttpResponseRedirect
from django.db.models import Sum
from django.utils.timezone import now

from models import Category
from models import Wallet
from models import RegularTransaction
from models import Transaction
from models import PaymentMethod
from models import Wish
import constants


class ModelAdminForOwner(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(ModelAdminForOwner, self).get_queryset(request)
        return qs.filter(owner=request.user)

    def save_model(self, request, obj, form, change):
        obj.owner = request.user
        obj.save()


class PaymentMethodAdmin(ModelAdminForOwner):
    readonly_fields = ('owner',)


class CategoryAdmin(ModelAdminForOwner):
    readonly_fields = ('owner',)


class WalletAdmin(ModelAdminForOwner):
    readonly_fields = ('owner',)


class BaseTransactionAdmin(ModelAdminForOwner):
    @csrf_protect_m
    def changelist_view(self, request, extra_context=None):
        from django.contrib.admin.views.main import ERROR_FLAG

        list_display = self.get_list_display(request)
        list_display_links = self.get_list_display_links(request, list_display)
        list_filter = self.get_list_filter(request)
        search_fields = self.get_search_fields(request)

        ChangeList = self.get_changelist(request)
        try:
            cl = ChangeList(request, self.model, list_display,
                list_display_links, list_filter, self.date_hierarchy,
                search_fields, self.list_select_related, self.list_per_page,
                self.list_max_show_all, self.list_editable, self)
        except IncorrectLookupParameters:
            # Wacky lookup parameters were given, so redirect to the main
            # changelist page, without parameters, and pass an 'invalid=1'
            # parameter via the query string. If wacky parameters were given
            # and the 'invalid=1' parameter was already in the query string,
            # something is screwed up with the database, so display an error
            # page.
            if ERROR_FLAG in request.GET.keys():
                return SimpleTemplateResponse('admin/invalid_setup.html', {
                    'title': _('Database error'),
                })
            return HttpResponseRedirect(request.path + '?' + ERROR_FLAG + '=1')

        extra_context = extra_context or {}

        qs = cl.get_queryset(request)
        extra_context['total_incomes'] = qs.filter(operation=1).aggregate(total=Sum('value'))['total'] or 0.
        extra_context['total_outcomes'] = qs.filter(operation=-1).aggregate(total=Sum('value'))['total'] or 0.
        extra_context['total_balance'] = extra_context['total_incomes'] - extra_context['total_outcomes']

        return super(BaseTransactionAdmin, self).changelist_view(request, extra_context)


class RegularTransactionAdmin(BaseTransactionAdmin):
    list_filter = ('day', 'month', 'wallet', 'category', 'operation', 'payment_method')
    list_display = ('name', 'real_value', 'day', 'month', 'wallet', 'payment_method')
    search_fields = ('name', 'description')
    ordering = ('month', 'day', 'weekday', '-operation')
    readonly_fields = ('owner',)


class TransactionAdmin(BaseTransactionAdmin):
    list_filter = ('date', 'wallet', 'category', 'operation', 'confirmed', 'date')
    list_display = ('name', 'real_value', 'formatted_date', 'wallet', 'category', 'confirmed')
    search_fields = ('name', 'description')
    ordering = ('date', '-operation')
    readonly_fields = ('owner', 'regularity', 'regular_transaction')
    actions = ['confirm_transactions']

    @csrf_protect_m
    def changelist_view(self, request, extra_context=None):
        # Months and years for filtering
        extra_context = extra_context or {}
        n = now()
        extra_context['current_month'] = n.month
        extra_context['current_year'] = n.year
        extra_context['months'] = [(unicode(k), unicode(v)) for k,v in constants.MONTH_CHOICES]
        extra_context['years'] = map(unicode, range(now().year - 2, now().year + 10))

        # When filtered by month and year, generates for such period
        date_month = request.GET.get('date__month', None)
        date_year = request.GET.get('date__year', None)
        if date_month and date_month.isdigit() and date_year and date_year.isdigit():
            start = datetime(int(date_year), int(date_month), 1)
            end = (start + timedelta(days=32)).replace(day=1)
            for regular in request.user.regular_transactions.all():
                regular.generate_for_period(start, end)

            # Also, returns wishes list for such month/year
            extra_context['wishes'] = request.user.wishes.filter(due_date__year=date_year,
                                                                 due_date__month=date_month)

        return super(TransactionAdmin, self).changelist_view(request, extra_context)

    def confirm_transactions(self, request, queryset):
        queryset.update(confirmed=True)
        return HttpResponseRedirect('/admin/budget/transaction/')

    def formatted_date(self, obj):
        if obj.date:
            return obj.date.strftime('%d/%m/%Y')
        else:
            return ''
    formatted_date.short_description = 'Date'


class WishAdmin(ModelAdminForOwner):
    list_display = ('name', 'cost', 'formatted_due_date', 'done')
    list_filter = ('done', 'due_date')
    search_fields = ('name', 'description')
    readonly_fields = ('owner',)

    def formatted_due_date(self, obj):
        if obj.due_date:
            return obj.due_date.strftime('%d/%m/%Y')
        else:
            return ''
    formatted_due_date.short_description = 'Due date'


admin.site.register(Category, CategoryAdmin)
admin.site.register(Wallet, WalletAdmin)
admin.site.register(RegularTransaction, RegularTransactionAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(PaymentMethod, PaymentMethodAdmin)
admin.site.register(Wish, WishAdmin)
