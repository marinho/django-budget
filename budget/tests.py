from datetime import datetime
from datetime import timedelta
from django.test import TestCase
from django.contrib.auth.models import User
from django.utils.timezone import utc
from models import RegularTransaction
from models import Wallet
from models import Category


class BudgetTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='tester')
        self.category = Category.objects.create(owner=self.user, name='Testing')
        self.wallet = Wallet.objects.create(owner=self.user, name='Testing')
        self.begin = datetime(2015, 6, 1, tzinfo=utc)
        self.end = datetime(2015, 8, 1, tzinfo=utc)

    def tearDown(self):
        self.user.delete()

    def test_generate_no_period_expense(self):
        regular = RegularTransaction.objects.create(
            name = 'Phone bill',
            owner = self.user,
            category = self.category,
            wallet = self.wallet,
            value = 19
        )
        regular.generate_for_period(self.begin, self.end)
        self.assertEqual(regular.transactions.count(), 0)

    def test_generate_weekly_expense(self):
        regular = RegularTransaction.objects.create(
            name = 'Phone bill',
            owner = self.user,
            category = self.category,
            wallet = self.wallet,
            value = 19,
            weekday = 3
        )
        regular.generate_for_period(self.begin, self.end)
        self.assertEqual(regular.transactions.count(), 9)

    def test_generate_monthly_expense(self):
        regular = RegularTransaction.objects.create(
            name = 'Phone bill',
            owner = self.user,
            category = self.category,
            wallet = self.wallet,
            value = 19,
            day = 3
        )
        regular.generate_for_period(self.begin, self.end)
        self.assertEqual(regular.transactions.count(), 2)

        # 2 already existing
        regular.generate_for_period(self.begin, self.end + timedelta(days=31))
        self.assertEqual(regular.transactions.count(), 3)

        # Confirmed isn't cleaned
        self.assertEqual(list(regular.transactions.values_list('id', flat=True)), [3, 4, 5])
        regular.transactions.filter(id=4).update(confirmed=True)
        regular.generate_for_period(self.begin, self.end + timedelta(days=31))
        self.assertEqual(list(regular.transactions.values_list('id', flat=True)), [4, 6, 7])
        self.assertEqual(regular.transactions.get(confirmed=True).id, 4)

    def test_generate_yearly_expense(self):
        regular = RegularTransaction.objects.create(
            name = 'Phone bill',
            owner = self.user,
            category = self.category,
            wallet = self.wallet,
            value = 19,
            day = 3,
            month = 2
        )
        # Empty for wrong month
        regular.generate_for_period(self.begin, self.end)
        self.assertEqual(regular.transactions.count(), 0)

        # Right month
        regular.month = 7
        regular.save()
        regular.generate_for_period(self.begin, self.end)
        self.assertEqual(regular.transactions.count(), 1)
