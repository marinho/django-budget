from django.db import models
from datetime import timedelta

from . import constants


class PaymentMethod(models.Model):
    """
    Payment methods are different ways a payment can be done.
    Examples: cash, transference, direct debt, bond, etc.
    """
    owner = models.ForeignKey('auth.User', related_name='payment_methods')
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)
    auto_confirmation = models.BooleanField(default=False, blank=True)

    def __unicode__(self):
        return self.name


class Category(models.Model):
    """
    Categories of expenses and transactions.
    Examples: salary, housing, gifts, benefits, leisure, etc.
    """
    class Meta:
        verbose_name_plural = 'Categories'

    owner = models.ForeignKey('auth.User', related_name='categories')
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class Wallet(models.Model):
    """
    Wallets represent accounts, such as bank accounts or investment wallets.
    """
    owner = models.ForeignKey('auth.User', related_name='wallets')
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class RegularTransaction(models.Model):
    """
    Regular transactions are scheduled planned transactions that generate real transactions
    Examples: monthly house rent, monthly wage, electricity bill, annual insurance, etc.
    """
    owner = models.ForeignKey('auth.User', related_name='regular_transactions')
    name = models.CharField(max_length=100)
    counterpart = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    value = models.DecimalField(max_digits=12, decimal_places=2)
    wallet = models.ForeignKey('Wallet', related_name='regular_transactions')
    payment_method = models.ForeignKey('PaymentMethod', related_name='regular_transactions', null=True, blank=True)
    category = models.ForeignKey('Category', related_name='regular_transactions')
    weekday = models.SmallIntegerField(blank=True, null=True, choices=constants.WEEKDAY_CHOICES)
    day = models.SmallIntegerField(blank=True, null=True, choices=constants.DAY_CHOICES)
    month = models.SmallIntegerField(blank=True, null=True, choices=constants.MONTH_CHOICES)
    operation = models.SmallIntegerField(default=constants.OPERATION_OUTCOME,
                                         choices=constants.OPERATION_CHOICES,
                                         db_index=True)

    def __unicode__(self):
        return self.name

    def real_value(self):
        return self.value * self.operation

    def match_date(self, date):
        # Week day
        if self.weekday is not None:
            if self.weekday != date.weekday():
                return False

        # Day
        elif self.day != date.day:
            return False

        # Month
        if self.month is not None and self.month != date.month:
            return False

        return True

    def generate_for_period(self, start, end):
        current = start
        while current < end:
            if self.match_date(current):
                existing = self.transactions.filter(owner=self.owner, date=current)

                if not existing.filter(confirmed=True).count():
                    existing.delete()
                    self.generate_transaction(current)

            current = current + timedelta(days=1)

    def generate_transaction(self, date):
        transaction, new = self.transactions.get_or_create(
            owner = self.owner,
            date = date,
            defaults = dict(
                name = self.name,
                counterpart = self.counterpart,
                value = self.value,
                wallet = self.wallet,
                payment_method = self.payment_method,
                category = self.category,
                operation = self.operation,
                description = self.description,
            )
        )
        return transaction


class Transaction(models.Model):
    """
    Transactions are real individual transactions, which can be generated automatically
    by Regular Transactions or just entered manually.
    """
    owner = models.ForeignKey('auth.User', related_name='transactions')
    name = models.CharField(max_length=100)
    counterpart = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    value = models.DecimalField(max_digits=12, decimal_places=2)
    wallet = models.ForeignKey('Wallet', related_name='transactions')
    payment_method = models.ForeignKey('PaymentMethod', related_name='transactions', null=True, blank=True)
    category = models.ForeignKey('Category', related_name='transactions')
    date = models.DateTimeField(db_index=True)
    operation = models.SmallIntegerField(default=constants.OPERATION_OUTCOME,
                                         choices=constants.OPERATION_CHOICES,
                                         db_index=True)
    confirmed = models.NullBooleanField(db_index=True)
    regular_transaction = models.ForeignKey('RegularTransaction',
                                            related_name='transactions',
                                            blank=True,
                                            null=True)
    regularity = models.CharField(max_length=10,
                                  db_index=True,
                                  blank=True,
                                  default=constants.REGULARITY_ONCE,
                                  choices=constants.REGULARITY_CHOICES)

    def __unicode__(self):
        return self.name

    def real_value(self):
        return self.value * self.operation

    def save(self, *args, **kwargs):
        # Auto confirmation
        if self.confirmed is None and self.payment_method and self.payment_method.auto_confirmation:
            self.confirmed = True

        # Regularity
        if not self.regular_transaction:
            self.regularity = constants.REGULARITY_ONCE
        elif not self.regular_transaction.month:
            self.regularity = constants.REGULARITY_MONTHLY
        else:
            self.regularity = constants.REGULARITY_YEARLY

        return super(Transaction, self).save(*args, **kwargs)


class Wish(models.Model):
    """
    This is a wish list object, to set wishes to make real
    Examples: cash, transference, direct debt, bond, etc.
    """
    class Meta:
        verbose_name_plural = 'Wishes'

    owner = models.ForeignKey('auth.User', related_name='wishes')
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)
    cost = models.DecimalField(max_digits=12, decimal_places=2)
    due_date = models.DateField(db_index=True, blank=True, null=True)
    done = models.BooleanField(default=False, blank=True, db_index=True)

    def __unicode__(self):
        return self.name
