# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('budget', '0003_transaction_confirmed'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='regular_transaction',
            field=models.ForeignKey(related_name='transactions', blank=True, to='budget.RegularTransaction', null=True),
        ),
    ]
