# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('budget', '0007_auto_20150813_0846'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentmethod',
            name='auto_confirmation',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='transaction',
            name='regularity',
            field=models.CharField(default=b'once', max_length=10, db_index=True, blank=True, choices=[(b'once', b'Once'), (b'monthly', b'Monthly'), (b'yearly', b'Yearly')]),
        ),
    ]
