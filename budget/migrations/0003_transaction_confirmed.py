# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('budget', '0002_auto_20150809_0050'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='confirmed',
            field=models.NullBooleanField(db_index=True),
        ),
    ]
