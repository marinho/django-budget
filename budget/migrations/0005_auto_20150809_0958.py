# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('budget', '0004_transaction_regular_transaction'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='counterpart',
            field=models.CharField(max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='regulartransaction',
            name='counterpart',
            field=models.CharField(max_length=100, blank=True),
        ),
    ]
