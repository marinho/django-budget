# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('budget', '0006_auto_20150811_0147'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentMethod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('description', models.TextField(blank=True)),
                ('owner', models.ForeignKey(related_name='payment_methods', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='regulartransaction',
            name='payment_method',
            field=models.ForeignKey(related_name='regular_transactions', blank=True, to='budget.PaymentMethod', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='payment_method',
            field=models.ForeignKey(related_name='transactions', blank=True, to='budget.PaymentMethod', null=True),
        ),
    ]
