# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('description', models.TextField(blank=True)),
                ('owner', models.ForeignKey(related_name='categories', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='RegularTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('counterpart', models.CharField(max_length=100)),
                ('description', models.TextField(blank=True)),
                ('value', models.DecimalField(max_digits=12, decimal_places=2)),
                ('weekday', models.SmallIntegerField(null=True, blank=True)),
                ('day', models.SmallIntegerField(null=True, blank=True)),
                ('month', models.SmallIntegerField(null=True, blank=True)),
                ('operation', models.SmallIntegerField(default=-1, db_index=True, choices=[(1, b'Income'), (-1, b'Outcome')])),
                ('category', models.ForeignKey(related_name='regular_transactions', to='budget.Category')),
                ('owner', models.ForeignKey(related_name='regular_transactions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('description', models.TextField(blank=True)),
                ('value', models.DecimalField(max_digits=12, decimal_places=2)),
                ('date', models.DateTimeField(db_index=True)),
                ('category', models.ForeignKey(related_name='transactions', to='budget.Category')),
                ('owner', models.ForeignKey(related_name='transactions', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Wallet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('description', models.TextField(blank=True)),
                ('owner', models.ForeignKey(related_name='wallets', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='transaction',
            name='wallet',
            field=models.ForeignKey(related_name='transactions', to='budget.Wallet'),
        ),
        migrations.AddField(
            model_name='regulartransaction',
            name='wallet',
            field=models.ForeignKey(related_name='regular_transactions', to='budget.Wallet'),
        ),
    ]
