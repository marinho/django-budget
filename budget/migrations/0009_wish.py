# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('budget', '0008_auto_20150813_0947'),
    ]

    operations = [
        migrations.CreateModel(
            name='Wish',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('description', models.TextField(blank=True)),
                ('cost', models.DecimalField(max_digits=12, decimal_places=2)),
                ('due_date', models.DateField(db_index=True, null=True, blank=True)),
                ('done', models.BooleanField(default=False, db_index=True)),
                ('owner', models.ForeignKey(related_name='wishes', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Wishes',
            },
        ),
    ]
