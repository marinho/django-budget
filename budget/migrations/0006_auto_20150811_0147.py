# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('budget', '0005_auto_20150809_0958'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regulartransaction',
            name='month',
            field=models.SmallIntegerField(blank=True, null=True, choices=[(None, b'Any month'), (1, 'January'), (2, 'February'), (3, 'March'), (4, 'April'), (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'), (10, 'October'), (11, 'November'), (12, 'December')]),
        ),
    ]
