from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^doc/', include('django.contrib.admindocs.urls')),
    url(r'', include(admin.site.urls)),
]
