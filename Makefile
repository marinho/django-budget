VENV_BIN="./env/bin/"

environ:
	virtualenv env
prepare:
	$(VENV_BIN)pip install -r requirements.txt
	$(VENV_BIN)python manage.py migrate
start: environ prepare
run: test
	$(VENV_BIN)python manage.py runserver 0.0.0.0:8000
shell:
	$(VENV_BIN)python manage.py shell
dbshell:
	$(VENV_BIN)python manage.py dbshell
test:
	$(VENV_BIN)python manage.py test
