Getting started
---------------

### First time

```
git clone ...
make start
make run
```

### Changes after first time

```
make prepare
```
